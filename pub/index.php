<?php
/**
 *--------------------------------------------------------------------------
 *
 *==========================================================================
 *
 *
*/
define('START_TIME', microtime(true));
define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', dirname(__DIR__).DS);
define('VENDOR_DIR', BASE_PATH.'vendor'.DS);
define('BLUEPRINT', BASE_PATH . 'blueprint.php');

/**
 *--------------------------------------------------------------------------
 *
 *==========================================================================
 *
 *
*/
require_once VENDOR_DIR . 'autoload.php';

/**
 *--------------------------------------------------------------------------
 *
 *==========================================================================
 *
 *
*/
global $BLUEPRINT; $BLUEPRINT = require BLUEPRINT;

/**
 *--------------------------------------------------------------------------
 *
 *==========================================================================
 *
 *
*/
$Application = require_once $BLUEPRINT['igniter'];

/**
 *--------------------------------------------------------------------------
 *
 *==========================================================================
 *
 *
*/

return $Application->run( START_TIME );
