# Overview
This framework is simply a 'blueprint' for your own framework. The file structure is 'loosely' created to allow you to change it to match your own structure. The creation of core packages is very 'loosely' created to allow you to replace them with your own packages. At the heart of it all is the MVC pattern, a very few 'required' files in 'expected' locations, but after you understand that, you can easily modify them also to meet your needs.
The reason for the existence of this framework, is to force a developer to comply with modern design patterns and utilize the latest technologies. Whenever we upgrade to a latter version, all you will need to do is modify your composer.json file to allow us to follow 'your' structure in upgrading your files. 


## Requirements
At least php 5.6, but recommend php 7 or greater

## Installation
    composer create-project phpframe/phpframe <dir> [dev-master, --prefer-dist]

## Configurations
Dig into the bluprint file and start creating the structure you want us to follow. After that, you just run myPHPframe, to modify the entire framework to comply with your structure.