<?php

if(!defined('START'))
	define('START', microtime(true));
if(!defined('DS'))
	define('DS', DIRECTORY_SEPARATOR);
if(!defined('BASE_PATH'))
	define('BASE_PATH', dirname(__DIR__).DS);
if(!defined('VENDOR_DIR'))
	define('VENDOR_DIR', BASE_PATH.'vendor'.DS);

/**************************************************************************************/



$blueprint['igniter']  = BASE_PATH.'sys'.DS.'boot'.DS. 'init.php';
$blueprint['runner']   = BASE_PATH.'sys'.DS.'boot'.DS. 'run.php';

$blueprint['configs']  = BASE_PATH.'sys'.DS.'conf';
$blueprint['routes']   = BASE_PATH.'app'.DS.'*'.DS.'http'.DS.'routes.php';

$blueprint['database'] = BASE_PATH.'sys'.DS.'conf'.DS.'database.php';

// $blueprint['templates'] = BASE_PATH.'app'.DS.'*'.DS.'views'.DS;
$blueprint['viewCache'] = BASE_PATH.'tmp'.DS.'cache'.DS;



/**************************************************************************************/
return $blueprint;
